/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// METMakerAlg.cxx

#include "METMakerAlg.h"
#include "METInterface/IMETMaker.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComposition.h"
#include "xAODMissingET/MissingETAssociationMap.h"

#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"

#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "TauAnalysisTools/ITauSelectionTool.h"

//add
#include "METUtilities/METHelpers.h"
#include "AthContainers/ConstDataVector.h"
#include "GaudiKernel/ITHistSvc.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "TFile.h"
#include "TTree.h"
#include "AsgTools/MessageCheck.h"
#include "AsgTools/AnaToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "METUtilities/CutsMETMaker.h"//
#include <xAODEventInfo/EventInfo.h>
#include "xAODTruth/TruthParticleContainer.h"

#   include "xAODRootAccess/Init.h"
#   include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfoAuxContainer.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
//weigthing events
#include "HepMC/GenEvent.h"
#include "PathResolver/PathResolver.h"
#include <TSystem.h>
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"

using std::string;
using namespace xAOD;

namespace met {

  typedef ElementLink<xAOD::IParticleContainer> iplink_t;
  static const SG::AuxElement::ConstAccessor< std::vector<iplink_t > > acc_constitObjLinks("ConstitObjectLinks");
  
  static SG::AuxElement::ConstAccessor<float> dec_avgMu("avgMu");
  static SG::AuxElement::ConstAccessor<float> dec_avgMunocorrection("AvgMunoCorrection");
  static SG::AuxElement::ConstAccessor<float> dec_pileupWeight("PileupWeight");
  static SG::AuxElement::ConstAccessor<char> acc_bjet("Bjet");
  static SG::AuxElement::ConstAccessor<float> acc_PileupWeight("PileupWeight");
  const static double Zmass = 91.2e3;

//	"lumicalc Files"
std::vector<std::string> listOfLumicalcFiles = {"GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"};
std::vector<std::string> listOfConfigFiles ={"GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root","/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363123.deriv.16082137.NTUP_PILEUP._000002.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363124.deriv.16082137.NTUP_PILEUP._000001.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363125.deriv.16082137.NTUP_PILEUP._000006.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363126.deriv.16082137.NTUP_PILEUP._000020.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363127.deriv.16082137.NTUP_PILEUP._000019.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363128.deriv.16082137.NTUP_PILEUP._000007.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363129.deriv.16082137.NTUP_PILEUP._000008.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363130.deriv.16082137.NTUP_PILEUP._000024.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363131.deriv.16082137.NTUP_PILEUP._000015.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363132.deriv.16082137.NTUP_PILEUP._000021.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363133.deriv.16082137.NTUP_PILEUP._000009.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363134.deriv.16082137.NTUP_PILEUP._000016.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363135.deriv.16082137.NTUP_PILEUP._000018.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363137.deriv.16082137.NTUP_PILEUP._000004.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363138.deriv.16082137.NTUP_PILEUP._000010.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363139.deriv.16082137.NTUP_PILEUP._000017.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363140.deriv.16082137.NTUP_PILEUP._000011.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363141.deriv.16082137.NTUP_PILEUP._000013.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363142.deriv.16082137.NTUP_PILEUP._000012.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363143.deriv.16082137.NTUP_PILEUP._000022.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363144.deriv.16082137.NTUP_PILEUP._000023.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363145.deriv.16082137.NTUP_PILEUP._000014.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363146.deriv.16082137.NTUP_PILEUP._000003.root",
"/METUtilities/Zmmprwconfigfilesg/user.mzaazoua.363136.deriv.16082137.NTUP_PILEUP._000005.root"};///zmm PRW config files for mc16d 

/*//ttbar PRWconfigfiles doesn't working
std::vector<std::string> listOfConfigFiles ={"GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root","/METUtilities/ttbarPRWConfFiles/user.mzaazoua.allttbarPRW_NTUP_PILEUP/user.mzaazoua.410470.deriv.16088887.NTUP_PILEUP._000001.root",
"/METUtilities/ttbarPRWConfFiles/user.mzaazoua.allttbarPRW_NTUP_PILEUP/user.mzaazoua.410470.deriv.16088887.NTUP_PILEUP._000003.root",
"/METUtilities/ttbarPRWConfFiles/user.mzaazoua.allttbarPRW_NTUP_PILEUP/user.mzaazoua.410470.deriv.16088887.NTUP_PILEUP._000004.root",
"/METUtilities/ttbarPRWConfFiles/user.mzaazoua.allttbarPRW_NTUP_PILEUP/user.mzaazoua.410470.deriv.16088887.NTUP_PILEUP._000005.root",
"/METUtilities/ttbarPRWConfFiles/user.mzaazoua.allttbarPRW_NTUP_PILEUP/user.mzaazoua.410647.deriv.16088887.NTUP_PILEUP._000002.root"};
*///
asg::AnaToolHandle<CP::IPileupReweightingTool> tool("CP::PileupReweightingTool/tool");

//////////////////////

//set other properties here

  //**********************************************************************
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl;
  METMakerAlg::METMakerAlg(const std::string& name,
			   ISvcLocator* pSvcLocator )
    : ::AthAlgorithm( name, pSvcLocator ),
    m_muonSelTool(""),
    m_elecSelLHTool(""),
    m_photonSelIsEMTool(""),
    m_tauSelTool(""),
    m_selTool("InDet::InDetTrackSelectionTool/TrackSelectionTool",this)
 //   m_grl ("GoodRunsListSelectionTool/grl", this)
 {
      for(unsigned int j=0;j<listOfConfigFiles.size();j++) {
            ATH_MSG_INFO("Locating File: " << listOfConfigFiles[j]);
file_conf.push_back(PathResolverFindCalibFile(listOfConfigFiles[j]));//.at(j)
      }

      for(unsigned int j=0;j<listOfLumicalcFiles.size();j++) {
            ATH_MSG_INFO("Locating File: " << listOfLumicalcFiles[j]);
file_lumi.push_back(PathResolverFindCalibFile(listOfLumicalcFiles[j]));//.at(j)		
      }

//    tool.setProperty("LumiCalcFiles",listOfLumicalcFiles);
 //   tool.setProperty("ConfigFiles",listOfConfigFiles);
    tool.setProperty("LumiCalcFiles", file_lumi);
    tool.setProperty("ConfigFiles", file_conf);

    declareProperty( "Maker",          m_metmaker                        );
    declareProperty( "METMapName",     m_mapname   = "METAssoc"          );
    declareProperty( "METCoreName",    m_corename  = "MET_Core"          );
    declareProperty( "METName",        m_outname   = "MET_Reference"     );

    declareProperty( "METSoftClName",  m_softclname  = "SoftClus"        );
    declareProperty( "METSoftTrkName", m_softtrkname = "PVSoftTrk"       );

    declareProperty( "InputJets",      m_jetColl   = "AntiKt4LCTopoJets" );//AntiKt4LCTopoJets  is the default value.  AntiKt4EMPflowJets for PFLow
    declareProperty( "InputElectrons", m_eleColl   = "Electrons"         );
    declareProperty( "InputPhotons",   m_gammaColl = "Photons"           );
    declareProperty( "InputTaus",      m_tauColl   = "TauJets"           );
    declareProperty( "InputMuons",     m_muonColl  = "Muons"             );

    declareProperty( "MuonSelectionTool",        m_muonSelTool           );
    declareProperty( "ElectronLHSelectionTool",  m_elecSelLHTool         );
    declareProperty( "PhotonIsEMSelectionTool" , m_photonSelIsEMTool     );
    declareProperty( "TauSelectionTool",         m_tauSelTool            );

    declareProperty( "DoTruthLeptons", m_doTruthLep = false              );//
    declareProperty("DoPRW",		 m_doPileupReweighting      ); //claire
    declareProperty( "TrackSelectionTool", m_selTool ); 
  }

  //**********************************************************************

  METMakerAlg::~METMakerAlg() { }

  //**********************************************************************

  StatusCode METMakerAlg::initialize() {
    ATH_MSG_INFO("Initializing " << name() << "...");
    ATH_MSG_INFO("Retrieving tools...");

    // retrieve tools
    if( m_metmaker.retrieve().isFailure() ) {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_metmaker->name());
      return StatusCode::FAILURE;
    };

    if( m_muonSelTool.retrieve().isFailure() ) {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_muonSelTool->name());
      return StatusCode::FAILURE;
    };

    if( m_elecSelLHTool.retrieve().isFailure() ) {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_elecSelLHTool->name());
      return StatusCode::FAILURE;
    };

    if( m_photonSelIsEMTool.retrieve().isFailure() ) {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_photonSelIsEMTool->name());
      return StatusCode::FAILURE;
    };

    if( m_tauSelTool.retrieve().isFailure() ) {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_tauSelTool->name());
      return StatusCode::FAILURE;
  ATH_CHECK(m_selTool.retrieve());
    };
 //   if( tool.retrieve().isFailure() ) {
//      ATH_MSG_ERROR("Failed to retrieve tool: " << tool->name());
//      return StatusCode::FAILURE;
 //   };

//add
    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK( histSvc.retrieve() ); 

    //create the file, the Tree and a few branches
    f = new TFile("tst_MET1.root","recreate");
    t = new TTree("t","t");
    CHECK( histSvc->regTree("/MYSTREAM/"+ name() +"t",t) );
    t->Branch("met_final",&met_final/*,"met_finalB/F"*/);
    t->Branch("mZ",&mZ/*,"met_finalB/F"*/);
    t->Branch("met_soft",&met_soft/*,"met_softD/F"*/);
    t->Branch("met_jet",&met_jet/*,"met_softD/F"*/);
    t->Branch("mettruth",&mettruth/*,"met_softD/F"*/);
    t->Branch("Default_Reco_NonIntMET",&Default_Reco_NonIntMET);
    t->Branch("avgMu", &m_avgmu, "avgMu/D");
    t->Branch("mu", &m_mu, "mu/D");
    t->Branch("correctedAvgMu", &m_correctedAvgMu/*,"correctedAvgMu/D"*/);
    t->Branch("ptZ", &m_ptZ, "ptZ/D");
    t->Branch("correctedAndScaledAvgMu",&m_correctedAndScaledAvgMu);
    t->Branch("PileupWeight", &m_Pileupweight, "PileupWeight/D");
    t->Branch("weight",&m_weight,"weight/D");
//pileup reason
    ttt = new TTree("ttt","ttt");
    CHECK( histSvc->regTree("/MYSTREAM/"+ name() +"ttt",ttt) );
    ttt->Branch("met_final_mu0_20",&met_final_mu0_20/*,"met_finalB/F"*/);
    ttt->Branch("mZ_mu0_20",&mZ_mu0_20/*,"met_finalB/F"*/);
    ttt->Branch("met_soft_mu0_20",&met_soft_mu0_20/*,"met_softD/F"*/);
    ttt->Branch("met_jet_mu0_20",&met_jet_mu0_20/*,"met_softD/F"*/);
    ttt->Branch("mettruth_mu0_20",&mettruth_mu0_20/*,"met_softD/F"*/);
    ttt->Branch("Default_Reco_NonIntMET_mu0_20",&Default_Reco_NonIntMET_mu0_20);
//
    ttt->Branch("met_final_mu20_40",&met_final_mu20_40/*,"met_finalB/F"*/);
    ttt->Branch("mZ_mu20_40",&mZ_mu20_40/*,"met_finalB/F"*/);
    ttt->Branch("met_soft_mu20_40",&met_soft_mu20_40/*,"met_softD/F"*/);
    ttt->Branch("met_jet_mu20_40",&met_jet_mu20_40/*,"met_softD/F"*/);
    ttt->Branch("mettruth_mu20_40",&mettruth_mu20_40/*,"met_softD/F"*/);
    ttt->Branch("Default_Reco_NonIntMET_mu20_40",&Default_Reco_NonIntMET_mu20_40);
//
    ttt->Branch("met_final_mu40_60",&met_final_mu40_60/*,"met_finalB/F"*/);
    ttt->Branch("mZ_mu40_60",&mZ_mu40_60/*,"met_finalB/F"*/);
    ttt->Branch("met_soft_mu40_60",&met_soft_mu40_60/*,"met_softD/F"*/);
    ttt->Branch("met_jet_mu40_60",&met_jet_mu40_60/*,"met_softD/F"*/);
    ttt->Branch("mettruth_mu40_60",&mettruth_mu40_60/*,"met_softD/F"*/);
    ttt->Branch("Default_Reco_NonIntMET_mu40_60",&Default_Reco_NonIntMET_mu40_60);
//
    ttt->Branch("met_final_mu60_80",&met_final_mu60_80/*,"met_finalB/F"*/);
    ttt->Branch("mZ_mu60_80",&mZ_mu60_80/*,"met_finalB/F"*/);
    ttt->Branch("met_soft_mu60_80",&met_soft_mu60_80/*,"met_softD/F"*/);
    ttt->Branch("met_jet_mu60_80",&met_jet_mu60_80/*,"met_softD/F"*/);
    ttt->Branch("mettruth_mu60_80",&mettruth_mu60_80/*,"met_softD/F"*/);
    ttt->Branch("Default_Reco_NonIntMET_mu60_80",&Default_Reco_NonIntMET_mu60_80);


  return StatusCode::SUCCESS;
  }

  //**********************************************************************

  StatusCode METMakerAlg::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
//m_metmaker->finalize();
    return StatusCode::SUCCESS;
  }

  //**********************************************************************

  StatusCode METMakerAlg::execute() {
    ATH_MSG_VERBOSE("Executing " << name() << "...");

    const xAOD::EventInfo* eventinfo = 0;
    ATH_CHECK( evtStore()->retrieve( eventinfo, "EventInfo" ) );
    ATH_MSG_DEBUG("Event");

 int dsid = 0;
 dsid = eventinfo->mcChannelNumber();
// std::string prwConfigFile = "dev/SUSYTools/PRW_AUTOCONFIG_SIM/files/dsid" + std::to_string(dsid) + "_FS.root";
// std::string prwConfigFile = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root";
 // std::string prwConfigFile("");
//    prwConfigFile += "/afs/cern.ch/work/m/mzaazoua/gitcache/MyProject/build/"mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.NTUP_PILEUP.e5271_e5984_s3126_r9781_r9778_p3384/NTUP_PILEUP.12739810.pool.root";

// prwConfigFile = PathResolverFindCalibFile(prwConfigFile);
//file_conf.push_back(PathResolverFindCalibFile(listOfConfigFiles));



ATH_CHECK(tool.retrieve());
//start pileup
      // Get the mu value for this event
        float mu = eventinfo->actualInteractionsPerCrossing();
        float avgmu =  eventinfo->averageInteractionsPerCrossing(); 
	float correctedAvgMu = eventinfo->auxdecor<float>("corrected_averageInteractionsPerCrossing");
//end pileup

//weighting sherpa 
tool->apply(*eventinfo);   /// don"t forget that for pileupweight the tool isnt applied yet!!
//tool->GetSumOfEventWeights(channelNumber);
    double weight=1;
    double mcweight = eventinfo->mcEventWeight();
//    std::cout<<"mcweight="<<mcweight<<std::endl;
float PileupWeight = tool->getCombinedWeight( *eventinfo );
       m_Pileupweight=PileupWeight;
       t->Fill();
//    std::cout<<"PileupWeight="<<PileupWeight<<std::endl;
    float pileupweight = eventinfo->auxdecor<float>("PileupWeight");
//    std::cout<<"pileupweight="<<pileupweight<<std::endl;
	    float mohameds_pileupweight = acc_PileupWeight(*eventinfo);    
	    //std::cout<<"mohameds_pileupweight====================="<<mohameds_pileupweight<<std::endl;
    weight = mcweight * PileupWeight;
m_weight=weight;
//    std::cout<<"weight="<<weight<<std::endl;
/////end weighting

float correctedAndScaledAvgMu = tool->getCorrectedAverageInteractionsPerCrossing( *eventinfo, true );
//std::cout<<"correctedAndScaledAvgMu="<<correctedAndScaledAvgMu<<std::endl;
m_correctedAndScaledAvgMu=correctedAndScaledAvgMu;
            t->SetWeight(weight);
	    t->Fill();

    // Create a MissingETContainer with its aux store
    MissingETContainer* newMet = new MissingETContainer();
    if( evtStore()->record(newMet, m_outname).isFailure() ) {
      ATH_MSG_WARNING("Unable to record MissingETContainer: " << m_outname);
      return StatusCode::SUCCESS;
    }
    MissingETAuxContainer* metAuxCont = new MissingETAuxContainer();
    if( evtStore()->record(metAuxCont, m_outname+"Aux.").isFailure() ) {
      ATH_MSG_WARNING("Unable to record MissingETAuxContainer: " << m_outname+"Aux.");
      return StatusCode::SUCCESS;
    }
    newMet->setStore(metAuxCont);

    const MissingETAssociationMap* metMap = 0;
    if( evtStore()->retrieve(metMap, m_mapname).isFailure() ) {
      ATH_MSG_WARNING("Unable to retrieve MissingETAssociationMap: " << m_mapname);
      return StatusCode::SUCCESS;
    }
    metMap->resetObjSelectionFlags();

    // Retrieve containers ***********************************************

    /// MET
    const MissingETContainer* coreMet(0);
    if( evtStore()->retrieve(coreMet, m_corename).isFailure() ) {
      ATH_MSG_WARNING("Unable to retrieve MissingETContainer: " << m_corename);
      return StatusCode::SUCCESS;
    }

    /// Jets
    const JetContainer* jetCont(0);
    if( evtStore()->retrieve(jetCont, m_jetColl).isFailure() ) {
      ATH_MSG_WARNING("Unable to retrieve input jet container: " << m_jetColl);
      return StatusCode::SUCCESS;
    }
///    ATH_MSG_DEBUG("Successfully retrieved jet collection");

    /// Electrons
    const ElectronContainer* elCont(0);
    if(!m_eleColl.empty()) {
      if( evtStore()->retrieve(elCont, m_eleColl).isFailure() ) {
	ATH_MSG_WARNING("Unable to retrieve input electron container: " << m_eleColl);
	return StatusCode::SUCCESS;
      }
///      ATH_MSG_DEBUG("Successfully retrieved electron collection");
    }

    /// Photons
    const PhotonContainer* phCont(0);
    if(!m_gammaColl.empty()) {
      if( evtStore()->retrieve(phCont, m_gammaColl).isFailure() ) {
	ATH_MSG_WARNING("Unable to retrieve input photon container: " << m_gammaColl);
	return StatusCode::SUCCESS;
      }
///      ATH_MSG_DEBUG("Successfully retrieved photon collection");
    }

    /// Taus
    const TauJetContainer* tauCont(0);
    if(!m_tauColl.empty()) {
      if( evtStore()->retrieve(tauCont, m_tauColl).isFailure() ) {
	ATH_MSG_WARNING("Unable to retrieve input tau container: " << m_tauColl);
	return StatusCode::SUCCESS;
      }
///      ATH_MSG_DEBUG("Successfully retrieved tau collection");
    }

    /// Muons
    const MuonContainer* muonCont(0);
    if(!m_muonColl.empty()) {
      if( evtStore()->retrieve(muonCont, m_muonColl).isFailure() ) {
	ATH_MSG_WARNING("Unable to retrieve input muon container: " << m_muonColl);
	return StatusCode::SUCCESS;
      }
///      ATH_MSG_DEBUG("Successfully retrieved muon collection");
    }

    // Select and flag objects for final MET building ***************************

    MissingETBase::UsageHandler::Policy objScale = MissingETBase::UsageHandler::PhysicsObject;
    if(m_doTruthLep) objScale = MissingETBase::UsageHandler::TruthParticle;
    // Electrons
    if(!m_eleColl.empty()) {
      ConstDataVector<ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
      for(const auto& el : *elCont) {
    	if(accept(el)) {
    	  metElectrons.push_back(el);
    	}
      }
      if( m_metmaker->rebuildMET("RefEle", xAOD::Type::Electron, newMet,
    				 metElectrons.asDataVector(),
    				 metMap, objScale).isFailure() ) {
    	ATH_MSG_WARNING("Failed to build electron term.");
      }
///      ATH_MSG_DEBUG("Selected " << metElectrons.size() << " MET electrons. "
///    		    << acc_constitObjLinks(*(*newMet)["RefEle"]).size() << " are non-overlapping.");
    }

    // Photons
    if(!m_gammaColl.empty()) {
      ConstDataVector<PhotonContainer> metPhotons(SG::VIEW_ELEMENTS);
      for(const auto& ph : *phCont) {
    	if(accept(ph)) {
    	  metPhotons.push_back(ph);
    	}
      }
      if( m_metmaker->rebuildMET("RefGamma", xAOD::Type::Photon, newMet,
    				 metPhotons.asDataVector(),
    				 metMap, objScale).isFailure() ) {
    	ATH_MSG_WARNING("Failed to build photon term.");
      }
///      ATH_MSG_DEBUG("Selected " << metPhotons.size() << " MET photons. "
///    		    << acc_constitObjLinks(*(*newMet)["RefGamma"]).size() << " are non-overlapping.");
    }

    // Taus
    if(!m_tauColl.empty()) {
      ConstDataVector<TauJetContainer> metTaus(SG::VIEW_ELEMENTS);
      for(const auto& tau : *tauCont) {
    	if(accept(tau)) {
    	  metTaus.push_back(tau);
    	}
      }
      if( m_metmaker->rebuildMET("RefTau", xAOD::Type::Tau, newMet,
    				 metTaus.asDataVector(),
    				 metMap, objScale).isFailure() ){
    	ATH_MSG_WARNING("Failed to build tau term.");
      }
///      ATH_MSG_DEBUG("Selected " << metTaus.size() << " MET taus. "
///    		    << acc_constitObjLinks(*(*newMet)["RefTau"]).size() << " are non-overlapping.");
    }

    // Muons
 bool ttbar=false;
 bool Zmmsel =false;
 int nMuons(0);   
    IParticle::FourMom_t Zboson;
   
    if(!m_muonColl.empty()) {
      ConstDataVector<MuonContainer> metMuons(SG::VIEW_ELEMENTS);
      for(const auto& mu : *muonCont) {
    	if(accept(mu)) {
    	  metMuons.push_back(mu);
	  //added
          ++nMuons;
	  //end	
    	}
      }
//        if(m_evttype==Zmm ){
if (nMuons==2){
    if ( metMuons.at(0)->charge() * metMuons.at(1)->charge() < 0 ) {
    Zboson = (metMuons.at(0)->p4() + metMuons.at(1)->p4()); 
    double best_Zmass = Zboson.M();
    m_ptZ=Zboson.Pt()/1000;
    if(fabs(best_Zmass-Zmass)<25e3){
    mZ=best_Zmass/1000;
    Zmmsel = true;
t->SetWeight(weight);
    t->Fill();
    }
 }

}
 /*   if(!m_muonColl.empty()) {
      ConstDataVector<MuonContainer> metMuons(SG::VIEW_ELEMENTS);
      for(const auto& mu : *muonCont) {
    	if(accept(mu)) {
    	  metMuons.push_back(mu);
    	}
      }*/
      
      if(m_doTruthLep) objScale = MissingETBase::UsageHandler::OnlyTrack;
      if( m_metmaker->rebuildMET("Muons", xAOD::Type::Muon, newMet,
    				 metMuons.asDataVector(),
    				 metMap, objScale).isFailure() ) {
    	ATH_MSG_WARNING("Failed to build muon term.");
      }
///      ATH_MSG_DEBUG("Selected " << metMuons.size() << " MET muons. "
///    		    << acc_constitObjLinks(*(*newMet)["Muons"]).size() << " are non-overlapping.");
    }

    if( m_metmaker->rebuildJetMET("RefJet", m_softclname, m_softtrkname, newMet,
				  jetCont, coreMet, metMap, true ).isFailure() ) {
      ATH_MSG_WARNING("Failed to build jet and soft terms.");
    }
///    ATH_MSG_DEBUG("Of " << jetCont->size() << " jets, "
///		  << acc_constitObjLinks(*(*newMet)["RefJet"]).size() << " are non-overlapping, "
///		  << acc_constitObjLinks(*(*newMet)[m_softtrkname]).size() << " are soft");

    MissingETBase::Types::bitmask_t trksource = MissingETBase::Source::Track;
    if((*newMet)[m_softtrkname]) trksource = (*newMet)[m_softtrkname]->source();
    if( m_metmaker->buildMETSum("FinalTrk", newMet, trksource).isFailure() ){
      ATH_MSG_WARNING("Building MET FinalTrk sum failed.");
    }
    MissingETBase::Types::bitmask_t clsource = MissingETBase::Source::LCTopo;
    if((*newMet)[m_softclname]) clsource = (*newMet)[m_softclname]->source();
    if( m_metmaker->buildMETSum("FinalClus", newMet, clsource).isFailure() ) {
      ATH_MSG_WARNING("Building MET FinalClus sum failed.");
    }
////////////////
if(selctZmm){
if( Zmmsel == true){

     MissingET* finalTrkMet  = new MissingET();
     newMet->push_back(finalTrkMet);
     *finalTrkMet = *(*newMet)["FinalTrk"];
     met_final=finalTrkMet->met()/1000;
     t->SetWeight(weight);
     t->SetWeight(weight);
     t->Fill();
//std::cout<<"finalTrkMet=   "<<finalTrkMet->met()/1000<<std::endl;
     MissingET* softmet  = new MissingET();
     newMet->push_back(softmet);
     *softmet = *(*newMet)[m_softtrkname];
//std::cout<<"softmet=   "<<softmet->met()/1000<<std::endl;
    met_soft=softmet->met()/1000;
    t->SetWeight(weight);
    t->Fill();
//jetterm
    MissingET* jetmet  = new MissingET();
    newMet->push_back(jetmet);
    *jetmet = *(*newMet)["RefJet"];
//std::cout<<"softmet=   "<<softmet->met()/1000<<std::endl;
    met_jet=jetmet->met()/1000;
    t->SetWeight(weight);
    t->Fill();

    const MissingETContainer* truth = 0;
//      ATH_CHECK( evtStore()->retrieve(truth, "MET_Truth") );
if (evtStore()->retrieve(truth, "MET_Truth").isFailure()) std::cout<<"failure to retrieve truth"<<std::endl;
//   const  MissingET* truth  = new MissingET();
      MissingET* met_truth = 0;

//      met_truth  = new MissingET();
//    MissingET* met_truth = 0;
//      *met_truth = *(*truth)["NonInt"];//"NonInt"
//       mettruth=met_truth->met()/1000;

  const xAOD::MissingETContainer* METTruths = 0;
  if( !/*m_event*/evtStore()->retrieve(METTruths, "MET_Truth") ) Error("execute()", "Failed to retrieve MET_Truth. Exiting.");
  const xAOD::MissingET* metTruth = 0;
  if( METTruths ){ metTruth = (*METTruths)["NonInt"];
       mettruth=metTruth->met()/1000;
       t->SetWeight(weight);
       t->Fill();
       Default_Reco_NonIntMET=(finalTrkMet->met()/1000-metTruth->met()/1000);
       t->SetWeight(weight);
       t->Fill();
       m_avgmu=avgmu;
       t->SetWeight(weight);
       t->Fill();
       m_mu=mu;
       t->SetWeight(weight);
       t->Fill();
       m_correctedAvgMu=correctedAvgMu;
       t->SetWeight(weight);
       t->Fill(); 
            
}
if(correctedAndScaledAvgMu>=0 && correctedAndScaledAvgMu<20){
			 met_final_mu0_20=finalTrkMet->met()/1000;
  			 met_soft_mu0_20=softmet->met()/1000;
			 met_jet_mu0_20=jetmet->met()/1000;
       			 mettruth_mu0_20=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu0_20=(finalTrkMet->met()/1000-	metTruth->met()/1000);
			 ttt->SetWeight(weight);
   			 ttt->Fill();
			}
if(correctedAndScaledAvgMu>=20 && correctedAndScaledAvgMu<40){
			 met_final_mu20_40=finalTrkMet->met()/1000;
  			 met_soft_mu20_40=softmet->met()/1000;
			 met_jet_mu20_40=jetmet->met()/1000;
       			 mettruth_mu20_40=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu20_40=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
   			 ttt->Fill();
			}
if(correctedAndScaledAvgMu>=40 && correctedAndScaledAvgMu<60){
			 met_final_mu40_60=finalTrkMet->met()/1000;
  			 met_soft_mu40_60=softmet->met()/1000;
			 met_jet_mu40_60=jetmet->met()/1000;
       			 mettruth_mu40_60=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu40_60=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
    			 ttt->Fill();
			}
if(correctedAndScaledAvgMu>=60 && correctedAndScaledAvgMu<80){
			 met_final_mu60_80=finalTrkMet->met()/1000;
  			 met_soft_mu60_80=softmet->met()/1000;
			 met_jet_mu60_80=jetmet->met()/1000;
       			 mettruth_mu60_80=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu60_80=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
    			 ttt->Fill();
			}
}
}//selctZmm fixed from .h
////////////////
  // select semilep ttbar events
  // require exactly 1 ele or exactly 1 muon
  if(selectttbar) 
  {
    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons,m_muonColl) );
    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons,m_eleColl) );
    ATH_MSG_VERBOSE (  " ttbar selection; num of good ele = "<< electrons->size());
    ATH_MSG_VERBOSE (  " ttbar selection; num of good muons = "<< muons->size());
//    if(   (electrons->size() + muons->size() )  != 1 ) return false; // HACK
    //if(   (electrons->size() + muons->size() )  != 1  || (electrons->size() + muons->size() )  != 2) return false;

    const JetContainer* jets = 0;
    ATH_CHECK( evtStore()->retrieve(jets,m_jetColl) );
//    ATH_MSG_VERBOSE (  " ttbar selection; num of good jets = "<< jets->size());
//    if(jets->size() < 4 ) return false; //HACK

    int countBJets = 0;
    for(const auto& iJet : *jets) {
//      		if (acc_bjet(*iJet)) ++countBJets;
    }   
//  if(!countBJets == 0 && !(jets->size() < 4) && (electrons->size() + muons->size())== 1){
		// Fill histograms
      MissingET* finalTrkMet  = new MissingET();
      newMet->push_back(finalTrkMet);
      *finalTrkMet = *(*newMet)["FinalTrk"];
      met_final=finalTrkMet->met()/1000;
      t->SetWeight(weight);
      t->Fill();
//std::cout<<"finalTrkMet=   "<<finalTrkMet->met()/1000<<std::endl;
      MissingET* softmet  = new MissingET();
      newMet->push_back(softmet);
      *softmet = *(*newMet)[m_softtrkname];
//std::cout<<"softmet=   "<<softmet->met()/1000<<std::endl;
      met_soft=softmet->met()/1000;
      t->SetWeight(weight);
      t->Fill();
//jetterm
      MissingET* jetmet  = new MissingET();
      newMet->push_back(jetmet);
      *jetmet = *(*newMet)["RefJet"];
//std::cout<<"softmet=   "<<softmet->met()/1000<<std::endl;
      met_jet=jetmet->met()/1000;
      t->SetWeight(weight);
      t->Fill();

      const MissingETContainer* truth = 0;
//    ATH_CHECK( evtStore()->retrieve(truth, "MET_Truth") );
      if (evtStore()->retrieve(truth, "MET_Truth").isFailure()) std::cout<<"failure to retrieve truth"<<std::endl;
//    const  MissingET* truth  = new MissingET();
      MissingET* met_truth = 0;

      const xAOD::MissingETContainer* METTruths = 0;
      if( !/*m_event*/evtStore()->retrieve(METTruths, "MET_Truth") ) Error("execute()", "Failed to retrieve MET_Truth. Exiting.");
      const xAOD::MissingET* metTruth = 0;
      if( METTruths ){ metTruth = (*METTruths)["NonInt"];
      	mettruth=metTruth->met()/1000;
      	t->SetWeight(weight);
      	t->Fill();
     	Default_Reco_NonIntMET=(finalTrkMet->met()/1000-metTruth->met()/1000);
      	t->SetWeight(weight);
      	t->Fill();
      	m_avgmu=avgmu;
      	t->SetWeight(weight);
      	t->Fill();
      	m_mu=mu;
      	t->SetWeight(weight);
      	t->Fill();
      }
      if(correctedAndScaledAvgMu>=0 && correctedAndScaledAvgMu<20){
			 met_final_mu0_20=finalTrkMet->met()/1000;
  			 met_soft_mu0_20=softmet->met()/1000;
			 met_jet_mu0_20=jetmet->met()/1000;
       			 mettruth_mu0_20=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu0_20=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
    ttt->Fill();
			}
      if(correctedAndScaledAvgMu>=20 && correctedAndScaledAvgMu<40){
			 met_final_mu20_40=finalTrkMet->met()/1000;
  			 met_soft_mu20_40=softmet->met()/1000;
			 met_jet_mu20_40=jetmet->met()/1000;
       			 mettruth_mu20_40=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu20_40=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
    			 ttt->Fill();
			}
      if(correctedAndScaledAvgMu>=40 && correctedAndScaledAvgMu<60){
			 met_final_mu40_60=finalTrkMet->met()/1000;
  			 met_soft_mu40_60=softmet->met()/1000;
			 met_jet_mu40_60=jetmet->met()/1000;
       			 mettruth_mu40_60=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu40_60=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
    			 ttt->Fill();
			}
if(correctedAndScaledAvgMu>=60 && correctedAndScaledAvgMu<80){
			 met_final_mu60_80=finalTrkMet->met()/1000;
  			 met_soft_mu60_80=softmet->met()/1000;
			 met_jet_mu60_80=jetmet->met()/1000;
       			 mettruth_mu60_80=metTruth->met()/1000;
			 Default_Reco_NonIntMET_mu60_80=(finalTrkMet->met()/1000-metTruth->met()/1000);
			 ttt->SetWeight(weight);
    			 ttt->Fill();
			}
//    }//end selection
  }//selectttbar fixed from .h
///////////////	
//}
    return StatusCode::SUCCESS;
  }

  //**********************************************************************

  bool METMakerAlg::accept(const xAOD::Muon* mu)
  {
    if( mu->pt()<25e3 || mu->pt()/cosh(mu->eta())<4e3 ) return false;
    return m_muonSelTool->accept(*mu);
  }

  bool METMakerAlg::accept(const xAOD::Electron* el)
  {
    if( fabs(el->eta())>2.47 || el->pt()<10e3 ) return false;
    return m_elecSelLHTool->accept(el);
  }

  bool METMakerAlg::accept(const xAOD::Photon* ph)
  {
    if( !(ph->author()&20) || fabs(ph->eta())>2.47 || ph->pt()<10e3 ) return false;
    return m_photonSelIsEMTool->accept(ph);
  }

  bool METMakerAlg::accept(const xAOD::TauJet* tau)
  { return m_tauSelTool->accept( *tau ); }

}
