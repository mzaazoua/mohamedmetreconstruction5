///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// METMakerAlg.h

#ifndef METMakerAlg_H
#define METMakerAlg_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODTau/TauJet.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
//add
#include "TFile.h"
#include "TTree.h"
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
//
class IMETMaker;
class IAsgElectronLikelihoodTool;
class IAsgPhotonIsEMSelector;
namespace CP {
  class IMuonSelectionTool;
}
namespace TauAnalysisTools {
  class ITauSelectionTool;
}
//tool for tracks
namespace InDet {
  class IInDetTrackSelectionTool;
}
//

namespace met {
  class METMakerAlg : public AthAlgorithm {

  public: 

    /// Constructor with parameters:
    METMakerAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~METMakerAlg(); 

    /// Athena algorithm's Hooks
    StatusCode  initialize();
    StatusCode  execute();
    StatusCode  finalize();

  private: 

    /// Default constructor:
    METMakerAlg();

    bool accept(const xAOD::Electron* el);
    bool accept(const xAOD::Photon* ph);
    bool accept(const xAOD::TauJet* tau);
    bool accept(const xAOD::Muon* muon);

    std::string m_mapname;
    std::string m_corename;
    std::string m_outname;

    std::string m_softclname;
    std::string m_softtrkname;

    std::string m_jetColl;
    std::string m_eleColl;
    std::string m_gammaColl;
    std::string m_tauColl;
    std::string m_muonColl;

    bool m_doTruthLep;
//add
   TTree* t;
   TFile* f;
  double met_final;
  double mZ;
  double met_soft;
  double met_jet;
  double mettruth;
  double Default_Reco_NonIntMET;
  double m_avgmu;
  double m_mu;
  bool m_doPileupReweighting; //claire
  bool selectttbar=false;
  bool selctZmm=true;
double m_correctedAvgMu;
double m_ptZ;
double m_correctedAndScaledAvgMu;
double m_Pileupweight;
double m_weight;
std::vector<std::string> m_prwLcalcFiles;
std::vector<std::string> m_prwConfFiles;
std::vector<std::string> prwConfigFiles;
std::vector<std::string> prwConfFiles;
std::vector<std::string> file_conf;
std::vector<std::string> file_lumi;
std::vector<std::string> m_prwFiles;
std::string file;


  TTree* ttt;//for pileup reason
//avgmu 0--->20
  double met_final_mu0_20;
  double mZ_mu0_20;
  double met_soft_mu0_20;
  double met_jet_mu0_20;
  double mettruth_mu0_20;
  double Default_Reco_NonIntMET_mu0_20;
//avgmu 20--->40
  double met_final_mu20_40;
  double mZ_mu20_40;
  double met_soft_mu20_40;
  double met_jet_mu20_40;
  double mettruth_mu20_40;
  double Default_Reco_NonIntMET_mu20_40;
//avgmu 40--->60
  double met_final_mu40_60;
  double mZ_mu40_60;
  double met_soft_mu40_60;
  double met_jet_mu40_60;
  double mettruth_mu40_60;
  double Default_Reco_NonIntMET_mu40_60;
//avgmu 60--->80
  double met_final_mu60_80;
  double mZ_mu60_80;
  double met_soft_mu60_80;
  double met_jet_mu60_80;
  double mettruth_mu60_80;
  double Default_Reco_NonIntMET_mu60_80;


    /// Athena configured tools
    ToolHandle<IMETMaker> m_metmaker;

    ToolHandle<CP::IMuonSelectionTool> m_muonSelTool;
    ToolHandle<IAsgElectronLikelihoodTool> m_elecSelLHTool;
    ToolHandle<IAsgPhotonIsEMSelector>     m_photonSelIsEMTool;
    ToolHandle<TauAnalysisTools::ITauSelectionTool> m_tauSelTool;

    ToolHandle<InDet::IInDetTrackSelectionTool> m_selTool;

  }; 

}

#endif
