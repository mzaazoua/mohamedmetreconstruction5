# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration


def getMETMakerAlg(suffix,jetSelection="Tier0",jetColl=""):
    from AthenaCommon import CfgMgr
#    from GaudiKernel.Constants import INFO #add

    print "Generate METMaker and METMakerAlg for METAssoc_"+suffix
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon.AlgSequence import AlgSequence
    topSequence = AlgSequence()


    doPFlow = 'PFlow' in suffix
    doTruth = suffix.startswith('Truth')
    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
                                    DoPFlow=doPFlow,
                                    DoSoftTruth=doTruth,
                                    JetSelection=jetSelection,
                                   );
#########tight########
#s7
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=2,
#				     JetWidthMuOlap=0.01,
#				     JetPsEMuOlap= 6e3,
#				     JetEmfMuOlap=1.5,
#				     JetTrkPtMuPt=1.2,
# 				     muIDPTJetPtRatioMuOlap=0.5,
#                                   );
#s6
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                     JetTrkNMuOlap=3,
#				     JetWidthMuOlap=0.025,
#				     JetPsEMuOlap= 4e3,
#				     JetEmfMuOlap=1,
#				     JetTrkPtMuPt=0.95,
# 				     muIDPTJetPtRatioMuOlap=1,
#                                   );
#S2
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=4,
#				     JetWidthMuOlap=0.05,
#				     JetPsEMuOlap= 3e3,
#				     JetEmfMuOlap=0.95,
#				     JetTrkPtMuPt=0.9,
# 				     muIDPTJetPtRatioMuOlap=1.5,
#                                   );
#####add more jet to the met "loose"###############
#S3
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=6,
#				     JetWidthMuOlap=0.15,
#				     JetPsEMuOlap= 2e3,
#				     JetEmfMuOlap=0.85,
#				     JetTrkPtMuPt=0.7,
# 				     muIDPTJetPtRatioMuOlap=2.5,
#                                   );
#S4
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=7,
#				     JetWidthMuOlap=0.2,
#				     JetPsEMuOlap= 1.5e3,
#				     JetEmfMuOlap=0.8,
#				     JetTrkPtMuPt=0.6,
# 				     muIDPTJetPtRatioMuOlap=3,
#                                   );
#S5
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=8,
#				     JetWidthMuOlap=0.25,
#				     JetPsEMuOlap= 1e3,
#				     JetEmfMuOlap=0.75,
#				     JetTrkPtMuPt=0.5,
# 				     muIDPTJetPtRatioMuOlap=3.5,
#                                   );
#####fix default chage others############
##best2
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=3,
#				     JetWidthMuOlap=0.25,
#				     JetPsEMuOlap=1.5e3,
#				     JetEmfMuOlap=0.85,
#				     JetTrkPtMuPt=0.95,
# 				     muIDPTJetPtRatioMuOlap=1.0,
#                                   );
#########s#################
##best1
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=2,
#				     JetWidthMuOlap=0.2,
#				     JetPsEMuOlap=2.0e3,
#				     JetEmfMuOlap=0.75,
#				     JetTrkPtMuPt=0.85,
# 				     muIDPTJetPtRatioMuOlap=1.5,
#                                   );
##best2
#   metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=4,
#				     JetWidthMuOlap=0.15,
#				     JetPsEMuOlap=1.0e3,
#				     JetEmfMuOlap=0.8,
#				     JetTrkPtMuPt=1.2,
#				     muIDPTJetPtRatioMuOlap=0.5,
#                                   );
##best3 
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                     JetTrkNMuOlap=5,
#				     JetWidthMuOlap=0.1,
#				     JetPsEMuOlap=2.5e3,
#				     JetEmfMuOlap=0.9,
#				     JetTrkPtMuPt=0.85,
#				     muIDPTJetPtRatioMuOlap=2.0,
#                                   );
##best5 not sure about vlues
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=5,
#				     JetWidthMuOlap=0.1,
#				     JetPsEMuOlap=2.5e3,
#				     JetEmfMuOlap=0.9,
#				     JetTrkPtMuPt=0.85,
#				     muIDPTJetPtRatioMuOlap=2.0,
#                                   );

##best1
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=3,
#				     JetWidthMuOlap=0.25,
#				     JetPsEMuOlap=1e3,
#				     JetEmfMuOlap=0.75,
#				     JetTrkPtMuPt=0.95,
# 				     muIDPTJetPtRatioMuOlap=1.5,
#                                   );
##best2
#    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
#                                    JetTrkNMuOlap=4,
#				     JetWidthMuOlap=0.2,
# 				     JetPsEMuOlap=1.5e3,
#				     JetEmfMuOlap=0.8,
#				     JetTrkPtMuPt=0.85,
#				     muIDPTJetPtRatioMuOlap=1.0,
#                                   );
##best3 
    metMaker = CfgMgr.met__METMaker('METMaker_'+suffix,
                                     JetTrkNMuOlap1=5,
				     JetTrkNMuOlap2=3,
				     JetWidthMuOlap=0.1,
				     JetPsEMuOlap=2.5e3,
				     JetEmfMuOlap=0.9,
				     JetTrkPtMuPt1=0.8,
				     JetTrkPtMuPt2=0.8,
				     muIDPTJetPtRatioMuOlap=30,
                                   );


#    tool.TrackSelectorTool = m_trkseltool
#    ToolSvc += m_trkseltool
    ToolSvc += metMaker

#    metRebuilder = CfgMgr.met__METRebuilder('METRebuilder_'+suffix,
#					     maxZ0SinTheta=0,
#					   );
#    ToolSvc += metRebuilder

    muonSel = CfgMgr.CP__MuonSelectionTool("MuonSelectionTool_METMakerAlg",
                                           MuQuality=1, # Medium
                                           MaxEta=2.4)
    ToolSvc += muonSel

    elecSelLH = CfgMgr.AsgElectronLikelihoodTool("EleSelLikelihood_METMakerAlg",
                                                 WorkingPoint="MediumLHElectron")
    ToolSvc += elecSelLH

    photonSelIsEM = CfgMgr.AsgPhotonIsEMSelector("PhotonSelIsEM_METMakerAlg",
                                                 WorkingPoint="TightPhoton")
    ToolSvc += photonSelIsEM

    tauSel = CfgMgr.TauAnalysisTools__TauSelectionTool("TauSelectionTool_METMakerAlg")
    ToolSvc += tauSel

    if jetColl=="":
        jetColl = suffix+'Jets'
        if doTruth:
            jetColl = suffix.split('_')[1]+'Jets'
    #add
    #metAlg.TrackSelectionTool.CutLevel = "LoosePrimary"
    #metAlg.TrackSelectionTool.maxZ0SinTheta =3 #1.5
    #metAlg.TrackSelectionTool.maxD0overSigmaD0 = 3
    
    #end
    makerAlg = CfgMgr.met__METMakerAlg('METMakerAlg_'+suffix,
                                       METMapName='METAssoc_'+suffix,
                                       METCoreName='MET_Core_'+suffix,
                                       METName='MET_Reference_'+suffix,
                                       InputJets=jetColl,
                                       Maker=metMaker,
                                       MuonSelectionTool=muonSel,
                                       ElectronLHSelectionTool=elecSelLH,
                                       PhotonIsEMSelectionTool=photonSelIsEM,
                                       TauSelectionTool=tauSel,
                                       )
    return makerAlg
